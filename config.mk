AB_OTA_PARTITIONS += \
    abl \
    aop \
    bluetooth \
    cmnlib \
    cmnlib64 \
    devcfg \
    dsp \
    featenabler \
    hyp \
    imagefv \
    keymaster \
    modem \
    multiimgoem \
    qupfw \
    tz \
    uefisecapp \
    xbl \
    xbl_config
