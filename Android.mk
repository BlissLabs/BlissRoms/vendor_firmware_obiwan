LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),obiwan)

FIRMWARE_IMAGES := $(wildcard $(LOCAL_PATH)/images/*)

$(foreach f, $(notdir $(FIRMWARE_IMAGES)), \
  $(call add-radio-file,images/$(f)))

include vendor/firmware/obiwan/config.mk

endif
